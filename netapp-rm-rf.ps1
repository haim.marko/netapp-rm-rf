<#
.SYNOPSIS
This is a  PowerShell script that delete data on volume mounted as netapp volume  

.DESCRIPTION
 
.EXAMPLE
.\netapp-rm-rf.ps1 -NetAppPathToDelete \\192.168.0.200\base\type\2019\09\26 -Type days -ProvisionBuffer 2
.\netapp-rm-rf.ps1 -NetAppPathToDelete 192.168.0.200:/base/type/2019/09/26 -Type days -ProvisionBuffer 2
.LINK

#>



Param (
    [Parameter(Mandatory=$True)]
    [String]$NetAppPathToDelete,
    [Parameter(Mandatory=$True)]
    [ValidateSet('days','months','years')]
    [String]$Type,
    [Parameter(Mandatory=$True)]
    [ValidateRange(1,5)]
    [Int]$ProvisionBuffer,
    [Parameter(Mandatory=$False)]
    [Boolean]$Force=$False
)
#LogFilePath 
$LogFilePath = 'C:\Temp\netapp-rm-rf.log'
#Get current date 
$BaseTime = (Get-Date)
#$BaseTime = (Get-Date).AddDays(12) #used for tesing 
#$BaseTime

#will be used as a parameter for Write-Log to specifiy and error 
$E = $True

#hash to host directories that already been created
$CreatedDirectories = @{}

#Minimum diffrence time from now to the required delete 
$MinDeltaForDeleteYears = 1*365
$MinDeltaForDeleteMonths = 2*30
$MinDeltaForDeleteDays = 3

#Maximum diffrence time from now to locate leftover volumes  
$MaxDeltaForLeftoversYears = 3*365
$MaxDeltaForLeftoversMonths = 5*30
$MaxDeltaForLeftoversDays = 10

#Permission while creating directory 
$DefaultDirPerm = 755

#Volume Properties 
$VolSizeYearly = '30t'
$VolSizeMonthly = '10t'
$VolSizeDaily = '5t'

$VolPrefix = 'autovol_'
$VolSnapPolicy = 'none'
$VolSnapReserve  = 0
$VolSpaceGuarantee = 'none'
$VolCommnetPrefix = 'Created_Automaticaly_At_'
$VolOfflineSuffix = '_offline_b4_delete_'
$VolOfflineRetentionDays = 5

#autosize settings 
$VolAutosizeMinSize = '5t'
$VolAutosizeGrowThreshold = '95'
$VolAutosizeShirinkThreshold = '60'
$VolAutosizeMaxSize = '30t'
$VolAutosizeMode = 'grow_shrink'

#remove trailing \ and / from the path
$NetAppPathToDelete = $NetAppPathToDelete -Replace "\\+$",""
$NetAppPathToDelete = $NetAppPathToDelete -Replace "/+$",""

function Write-Log ($log,$e,$notime){
    if ($log) {
        if (!$notime) {
            $log = "$(get-date) - $log" # <--- Prompt to screen.
        }
        if ($e) {
            Write-Host "ERROR: $log" -ForegroundColor Red
           "ERROR: $log" | Out-File $LogFilePath -Append
            exit(1)
        } else {
            Write-Host "$log"
            $log | Out-File $LogFilePath -Append
        }
    }
}

function DoesNcDirExists ($VolPath) {
    $o = Read-NcDirectory -Path $VolPath -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Dirs    
    if ($Err) {
        if ($Err.CategoryInfo.Reason -eq 'EONTAPI_ENOENT') {
            Return $False
        } else {
            Write-Log "Error getting path info: $DirPath" $E
        }
    } 
    Return $True
}

function CreateNcDirIfNotExists ($DirPath, $VolPath) {

    $VolPath = '/vol/'+$VolPath

    if (-not $CreatedDirectories.ContainsKey($VolPath)) {
        Write-Log "Validating dir exists: $DirPath ($VolPath)"
        if (-not $(DoesNcDirExists $VolPath)) {
            Write-Log "Path: $DirPath ($VolPath) does not exists, creating"
            $o = New-NcDirectory -Path $VolPath -Permission $DefaultDirPerm -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Dirs    
            if ($Err) {
                Write-Log "Error creating directory: $DirPath" $E
            }
            $CreatedDirectories.Add($VolPath,1)
            Write-Log "Directory created: $DirPath ($VolPath)"
        } else {
            Write-Log "Dir already exists: $DirPath ($VolPath)"    
            $CreatedDirectories.Add($VolPath,1)
        }  
    }
}

function CreateVolAndMount ($VolName, $MountPoint, $VolSize, $Policy, $SecStyle) {
    $o = Get-NcVserverAggr -Controller $NCConnecion -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Aggr | Sort-Object $_.Available
    if ($Err) {
        Write-Log "Could not get volume list of aggregates from: $NetAppHost - $($Err.Exception)" $E
    }

    if (-not $Aggr) {
        Write-Log "No aggregates are assigned to the SVM: $NetAppHost please user the vserver add-aggregate command to add aggregates to this SVM" $E
    }

    $AggrName = $Aggr[0].AggregateName
    $VolComment = $VolCommnetPrefix+$BaseTime.Day.ToString("00")+$BaseTime.Month.ToString("00")+$BaseTime.Year.ToString("0000")

    #Creating Volume 
    Write-Log "Creating volume: $VolName on Aggregate: $AggrName Size: $VolSize MountPoint: $MountPoint ExportPolicy: $Policy SecStyle: $SecStyle"
    $o = New-NcVol -Controller $NCConnecion -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable NewVol `
        -Name $VolName -Aggregate $AggrName -Size $VolSize -SnapshotPolicy $VolSnapPolicy -SnapshotReserve $VolSnapReserve `
         -SpaceGuarantee $VolSpaceGuarantee -Comment $VolComment -JunctionPath $MountPoint -ExportPolicy $Policy -SecurityStyle $SecStyle
    
    if ($Err) {
        Write-Log "Could not create volume: $NetAppHost - $($Err.Message)$($Err.Exception.Message)" $E
    }    
    
    Write-Log "Setting autosize on volume: $VolName"
    $o = Set-NcVolAutosize -Name $VolName -MinimumSize $VolAutosizeMinSize -GrowThresholdPercent $VolAutosizeGrowThreshold -ShrinkThresholdPercent $VolAutosizeShirinkThreshold -MaximumSize $VolAutosizeMaxSize -Mode $VolAutosizeMode -ErrorVariable Err -ErrorAction SilentlyContinue
    if ($Err) {
        Write-Log "Could not set autosize on volume: $NetAppHost - $($Err.Message)$($Err.Exception.Message)" $E
    }        
}

#Validate ProvisionBuffer 
if ($ProvisionBuffer -gt 2 -and $Type -eq 'years') {
    Write-Log "for years type ProvisionBuffer should be <=2" $E
}

#Load DataOntap module 
$module = Get-Module DataONTAP
if ($module -eq $null) {
    $module = Import-Module -Name DataONTAP -PassThru
    if ( $module -eq $null ) {
        Write-Log "DataONTAP module not found" $E
        
    }
}

$Protocol = ''
if ($NetAppPathToDelete.split(':').count -gt 1) {
    ($NetAppHost,$NetAppJunction) = $NetAppPathToDelete.split(':')
    $Protocol = 'NFS'
}

if ($NetAppPathToDelete.StartsWith('\\')) {
    $NetAppHost = $NetAppPathToDelete.Split('\')[2]
    $NetAppShare = $NetAppPathToDelete.Split('\')[3]
    $PathPrefix = '\\'+$NetAppHost+'\'+$NetAppShare
    $CifsPath = $NetAppPathToDelete.Replace($PathPrefix,'')
    if ($CifsPath -eq '\') {$CifsPath = ''}
    $Protocol = 'CIFS'
}

$o = Get-NcCredential -name $NetAppHost -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Cred
if (-not $Cred -or $Err) {
    Write-Log "Could not find cached credenitals for NetApp SVM: $NetAppHost, plese use Add-NCCredential Cmdlet on the server" $E
}

$NCConnecion = Connect-NcController -name $NetAppHost
if ($Err){
    Write-Log "Could not connect to NetApp SVM: $NetAppHost - $($Err.Exception)" $E
}

if ($NCConnecion.Vserver -eq "") {
    Write-Log "Could not identify NetApp as SVM" $E   
}

if ($Protocol-eq 'CIFS') {
    if (-not $NetAppShare -or -not $CifsPath) {
        Write-Log "CIFS Path is not legal: $NetAppPathToDelete (need to be \\netapp\share\folder..)" $E       
    }
    $o = Get-NCCifsShare -Controller $NCConnecion -Name $NetAppShare -ErrorVariable Out -ErrorAction SilentlyContinue -OutVariable Shares
    if (-not $Shares) {
        Write-Log "CIFS share: $PathPrefix does not exists" $E
    }

    $SharePath = $Shares[0].Path 
    $NetAppJunction = $SharePath + $CifsPath.Replace('\','/')
    Write-Log "Junction path for $NetAppPathToDelete is: $NetAppJunction"
}

if ($Protocol-eq 'NFS') {
    if (-not $NetAppJunction -or -not $NetAppJunction.StartsWith('/')) {
        Write-Log "NFS Path is not legal: $NetAppJunction (need to be /vol/type/folder" $E       
    }
    Write-Log "Junction path for $NetAppPathToDelete is: $NetAppJunction"
}

if ($type -eq 'years' -and $NetAppJunction -match '(.+)\/(.+)\/(\d+)$') {
    $YearJunction = $NetAppJunction.Split('/')[-1]
    $TypeJunction = $NetAppJunction.Split('/')[-2]
    $BaseJunction = $NetAppJunction -replace $('/'+$TypeJunction+'/'+ $YearJunction)
}

if ($type -eq 'months' -and $NetAppJunction -match '(.+)\/(.+)\/(\d+)\/(\d+)$') {
    $MonthJunction = $NetAppJunction.Split('/')[-1]
    $YearJunction = $NetAppJunction.Split('/')[-2]
    $TypeJunction = $NetAppJunction.Split('/')[-3]
    $BaseJunction = $NetAppJunction -replace $('/'+$TypeJunction+'/'+$YearJunction+'/'+$MonthJunction)
}

if ($type -eq 'days' -and $NetAppJunction -match '(.+)\/(\S+)\/(\d+)\/(\d+)\/(\d+)$') {
    $DayJunction = $NetAppJunction.Split('/')[-1]
    $MonthJunction = $NetAppJunction.Split('/')[-2]
    $YearJunction = $NetAppJunction.Split('/')[-3]
    $TypeJunction = $NetAppJunction.Split('/')[-4]    
    $BaseJunction = $NetAppJunction -replace $('/'+$TypeJunction+'/'+$YearJunction+'/'+$MonthJunction+'/'+$DayJunction)
}

#Validate path structure
if ($Type -eq 'years' -and (($BaseJunction -eq $null -or $TypeJunction -eq $null -or $YearJunction -eq $null) -or ($MonthJunction -ne $null -or $DayJunction -ne $null))) {
    Write-Log "Provided path: $NetAppPathToDelete  does not comply with $Type type" $E      
}

if ($Type -eq 'months' -and (($BaseJunction -eq $null -or $TypeJunction -eq $null -or $YearJunction -eq $null -or $MonthJunction -eq $null) -or ($DayJunction -ne $null))) {
    Write-Log "Provided path: $NetAppPathToDelete  does not comply with $Type type" $E      
}

if ($Type -eq 'days' -and ((($BaseJunction -eq $null -or $TypeJunction -eq $null -or $YearJunction -eq $null -or $MonthJunction -eq $null -or $DayJunction -eq $null)))) {
    Write-Log "Provided path: $NetAppPathToDelete  does not comply with $Type type" $E      
}

#calculate the requested delete date 
if ($YearJunction -eq $null) {$y=$BaseTime.Year} else {$y=[int]$YearJunction}
if ($MonthJunction -eq $null) {$m=$BaseTime.Month} else {$m=[int]$MonthJunction}
if ($DayJunction -eq $null) {$d=$BaseTime.Day} else {$d=[int]$DayJunction}
$DateToDelete = Get-Date -Year $y -Month $m -Day $d -ErrorAction SilentlyContinue -ErrorVariable Err
if (-not $DateToDelete -or $y -lt 2010) {
    Write-Log "Provided path: $NetAppPathToDelete does not comply with $Type type (cannot create legal date structure out of $([String]$d+"-"+[String]$m+"-"+[String]$y))" $E         
}

#calulate the delta between now to the requested delete date
if ($Type -eq 'years') {$RelativeDelta = $MinDeltaForDeleteYears}
if ($Type -eq 'months') {$RelativeDelta = $MinDeltaForDeleteMonths}
if ($Type -eq 'days') {$RelativeDelta = $MinDeltaForDeleteDays}
$DateDelta = New-TimeSpan -Start $DateToDelete -End $BaseTime

$o = Get-NCVol -Controller $NCConnecion -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Volumes
if ($Err) {
    Write-Log "Could not get volume list from: $NetAppHost - $($Err.Exception)" $E
}


$BaseVol = $($Volumes | ? {$_.JunctionPath -eq $BaseJunction})
if (-not $BaseVol) {
    Write-Log "Could not get volume name for base path: $BaseVol" $E   
}

CreateNcDirIfNotExists $($BaseJunction+'/'+$TypeJunction) $($BaseVol.Name+'/'+$TypeJunction)

Write-Log "Provisioning future directories"
for ($i=0; $i -lt $ProvisionBuffer; $i++) {

    if ($Type -eq 'days') {
        $CreateTime = $BaseTime.AddDays($i)
        #Creating required directories 
        CreateNcDirIfNotExists $($BaseJunction+'/'+$TypeJunction+'/'+[string]$CreateTime.Year) $($BaseVol.Name+'/'+$TypeJunction+'/'+[string]$CreateTime.Year)
        CreateNcDirIfNotExists $($BaseJunction+'/'+$TypeJunction+'/'+[string]$CreateTime.Year+'/'+$CreateTime.Month.ToString("00")) $($BaseVol.Name+'/'+$TypeJunction+'/'+[string]$CreateTime.Year+'/'+$CreateTime.Month.ToString("00"))        
        
        #Creating Required Volume and mounting it on the location 
        $VolNameToCreate = $VolPrefix+$TypeJunction+'_'+[string]$CreateTime.Year+'_'+$CreateTime.Month.ToString("00")+'_'+$CreateTime.Day.ToString("00")
        $MountVolOn = $BaseJunction+'/'+$TypeJunction+'/'+[string]$CreateTime.Year+'/'+$CreateTime.Month.ToString("00")+'/'+$CreateTime.Day.ToString("00")
        $MountVolOnVolPath = '/vol/'+$BaseVol+'/'+$TypeJunction+'/'+[string]$CreateTime.Year+'/'+$CreateTime.Month.ToString("00")+'/'+$CreateTime.Day.ToString("00")
    }

    if ($Type -eq 'months') {
        $CreateTime = $BaseTime.AddMonths($i)
        #Creating required directories 
        CreateNcDirIfNotExists $($BaseJunction+'/'+$TypeJunction+'/'+[string]$CreateTime.Year) $($BaseVol.Name+'/'+$TypeJunction+'/'+[string]$CreateTime.Year)
        
        #Creating Required Volume and mounting it on the location 
        $VolNameToCreate = $VolPrefix+$TypeJunction+'_'+[string]$CreateTime.Year+'_'+$CreateTime.Month.ToString("00")
        $MountVolOn = $BaseJunction+'/'+$TypeJunction+'/'+[string]$CreateTime.Year+'/'+$CreateTime.Month.ToString("00")
        $MountVolOnVolPath = '/vol/'+$BaseVol+'/'+$TypeJunction+'/'+[string]$CreateTime.Year+'/'+$CreateTime.Month.ToString("00")
    }

    if ($Type -eq 'years') {
        $CreateTime = $BaseTime.AddYears($i)
        
        #Creating Required Volume and mounting it on the location 
        $VolNameToCreate = $VolPrefix+$TypeJunction+'_'+[string]$CreateTime.Year
        $MountVolOn = $BaseJunction+'/'+$TypeJunction+'/'+[string]$CreateTime.Year
        $MountVolOnVolPath = '/vol/'+$BaseVol+'/'+$TypeJunction+'/'+[string]$CreateTime.Year
    }

    #check if volume already exists and mounted 
    if ($Volumes | ? {$_.Name -eq $VolNameToCreate -and $_.JunctionPath -eq $MountVolOn}) {
        Write-Log "Volume $VolNameToCreate exists and mounted on $($MountVolOn), skipping"
    # check if there is lower level volume mounted on the same location 
    } elseif ($Volumes | ? {$_.JunctionPath -ne $Null -and ($_.JunctionPath.StartsWith($($MountVolOn+'/')) -or $_.JunctionPath -eq $MountVolOn)}) {
        Write-Log "Cannot create volume: $VolNameToCreate , there are volumes mounted on the same or deeper junction: $MountVolOn" $E
    #check if junction path exists as directory 
    } elseif (DoesNcDirExists $MountVolOnVolPath -eq $True) {
        Write-Log "Cannot create volume: $VolNameToCreate , junction path already exists as directory: $MountVolOn ($MountVolOnVolPath)"
    } else {
        CreateVolAndMount $VolNameToCreate $MountVolOn $VolSizeDaily $BaseVol.VolumeExportAttributes.Policy $BaseVol.VolumeSecurityAttributes.Style
    }
}

if ($DateDelta.Days -lt $RelativeDelta) {
     Write-Log "Requested to delete data which is newer than allowed (Delta from now is:$($DateDelta.Days) days while allow delta is:$($RelativeDelta) days)" $E    
}

$o = Get-NCVol -Controller $NCConnecion -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Volumes
if ($Err) {
    Write-Log "Could not get volume list from: $NetAppHost - $($Err.Exception)" $E
}


Write-Log "Removing volume for junction path: $NetAppJunction"
$VolObj = $Volumes | ? {$_.JunctionPath -eq $NetAppJunction}
if ($VolObj) {
    if (-not $VolObj.VolumeIdAttributes.Comment.StartsWith($VolCommnetPrefix)) {
        Write-Log "Volume: $($VolObj.Name) wasn't created using this automation (expected commnet field could not be found)" $E
    }
    Write-Log "Unmounting volume: $($VolObj.Name) Junction Path: $($VolObj.JunctionPath) before taking it offline"
    $o = Dismount-NcVol -Name $VolObj.Name -Force:$True -ErrorVariable Err -ErrorAction SilentlyContinue
    if ($Err) {
        Write-Log "Could not unmount volume: $($VolObj.Name) - $($Err.Message)$($Err.Exception.Message)" $E
    }

    $DeleteTime = $BaseTime.AddDays($VolOfflineRetentionDays)
    $OfflineVolName = $VolObj.Name + $VolOfflineSuffix +$DeleteTime.Day.ToString("00")+$DeleteTime.Month.ToString("00")+$DeleteTime.Year.ToString("0000")
    
    Write-Log "Renaming volume: $($VolObj.Name) to: $($OfflineVolName)"
    $o = Rename-NcVol -Name $VolObj.Name -NewName $OfflineVolName -ErrorVariable Err -ErrorAction SilentlyContinue
    if ($Err) {
        Write-Log "Could not rename volume: $($VolObj.Name) to: $($OfflineVolName) - $($Err.Message)$($Err.Exception.Message)" $E
    }

    Write-Log "Offlining volume: $($OfflineVolName)"
    $o = Set-NcVol -Name $OfflineVolName -Offline:$True -ErrorVariable Err -ErrorAction SilentlyContinue
    if ($Err) {
        Write-Log "Could not offline volume: $($OfflineVolName) - $($Err.Message)$($Err.Exception.Message)" $E
    }
} else {
    Write-Log "WARNING: No volume found that matches the required junction path" 
}

Write-Log "Deleting old offlined volumes"
$o = Get-NCVol -Controller $NCConnecion -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Volumes
if ($Err) {
    Write-Log "Could not get volume list from: $NetAppHost - $($Err.Exception)" $E
}

$OfflineVolumes = $Volumes | ? {$_.State -eq 'offline' -and $_.VolumeIdAttributes.Comment.StartsWith($VolCommnetPrefix) -and $_.Name.Contains($VolOfflineSuffix)}
if ($OfflineVolumes) {
    $OfflineVolumes | Foreach-Object {
        $VolName = $_.Name   
        if ($VolName -match "(\d{2})(\d{2})(\d{4})$") {
            $DeleteDate = Get-Date -Day $matches[1] -Month $matches[2] -Year $matches[3]
            if ($DeleteDate) {
                $DateDelta = New-TimeSpan -Start  $BaseTime -End $DeleteDate
                if ($DateDelta.Days -le 0) {
                   
                    Write-Log "Deleting volume: $VolName (delete date was set to $($DeleteDate.Day)-$($DeleteDate.Month)-$($DeleteDate.Year))" 
                    $o = Remove-NcVol -Name $VolName -Confirm:$False -ErrorVariable Err -ErrorAction SilentlyContinue
                    if ($Err) {
                        Write-Log "Could not delete volume: $($VolName) - $($Err.Message)$($Err.Exception.Message)" $E
                    }                    
                } else {
                    Write-Log "Volume: $($VolName) delete retention not reached yet (will reach in $($DateDelta.Days) days)"
                }
            } else {
                Write-Log "Cannot extact delete date from volume: $VolName"             
            }
        }
    }
} else {
    Write-Log "No volumes need to be deleted" 
}

Write-Log "Looking for leftovers volumes that probebly should be deleted"

$o = Get-NCVol -Controller $NCConnecion -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Volumes
if ($Err) {
    Write-Log "Could not get volume list from: $NetAppHost - $($Err.Exception)" $E
}

$Found = $False
$OnlineVolumes = $Volumes | ? {$_.State -eq 'online' -and $_.VolumeIdAttributes.Comment -ne $null -and $_.VolumeIdAttributes.Comment.StartsWith($VolCommnetPrefix)}
if ($OnlineVolumes) {
    $OnlineVolumes | Foreach-Object {
        $VolName = $_.Name  
        $VolDate = $Null
        if ($VolName -match "(\d{4})$") {
            $VolDate = Get-Date -Year $matches[1] -Month $BaseTime.Month -Day $BaseTime.Day
            $MaxDelta = $MaxDeltaForLeftoversYears
        }
        if ($VolName -match "(\d{4})_(\d{2})$") {
            $VolDate = Get-Date -Year $matches[1] -Month $matches[2] -Day $BaseTime.Day
            $MaxDelta = $MaxDeltaForLeftoversMonths
        }
        if ($VolName -match "(\d{4})_(\d{2})_(\d{2})$") {
            $VolDate = Get-Date -Year $matches[1] -Month $matches[2] -Day $matches[3]
            $MaxDelta = $MaxDeltaForLeftoversDays
        }        

        if ($VolDate) {
            $DateDelta = New-TimeSpan -Start $VolDate -End $BaseTime
            if ($DateDelta.Days -ge $MaxDelta) {
                Write-Log "WARNING: volume: $VolName is probebly online leftover (it's date is $($DateDelta.Days) days ago). please remove it" 
                $Found = $True
            } 
        } else {
            Write-Log "Cannot extact delete date from volume: $VolName"             
        }
    }
    if (-not $Found) {
        Write-Log "No leftover volumes were found" 
    }
} else {
    Write-Log "No leftover volumes were found" 
}    

